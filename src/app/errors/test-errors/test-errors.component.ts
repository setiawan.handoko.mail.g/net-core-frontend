import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-test-errors',
  templateUrl: './test-errors.component.html',
  styleUrls: ['./test-errors.component.css']
})
export class TestErrorsComponent implements OnInit {
  
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  get404Error() {
    this.http.get(this.baseUrl + 'errorhandling/not-found').subscribe(response => {
      console.log(response);
    }, err => {
      console.log(err);
    })
  }

  get400Error() {
    this.http.get(this.baseUrl + 'errorhandling/bad-request').subscribe(response => {
      console.log(response);
    }, err => {
      console.log(err);
    })
  }

  get500Error() {
    this.http.get(this.baseUrl + 'errorhandling/auth').subscribe(response => {
      console.log(response);
    }, err => {
      console.log(err);
    })
  }

  get404ValidationError() {
    this.http.get(this.baseUrl + 'errorhandling/not-found').subscribe(response => {
      console.log(response);
    }, err => {
      console.log(err);
    })
  }

}
