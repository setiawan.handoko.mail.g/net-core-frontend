import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, of as observableOf } from 'rxjs';
import { AccountService } from '../_services/account.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private accountService: AccountService, private toastr: ToastrService) {}

  canActivate(): Observable<boolean> {
    var ls = localStorage.getItem('user');
    if(ls){
      return observableOf(true);
    } 
    this.toastr.error('Not Allowed!');
    return observableOf(false);
    

    // var bol = this.accountService.currentUser$.pipe(
    //   map(user => {
    //     if(user) return true;
    //     return false;
    //   })
    // )
    // console.log(bol);

    // if(bol) {
    //   return observableOf(true);
    // }
    // this.toastr.error('Not Allowed!');
    // return observableOf(false);
  }
  
}
