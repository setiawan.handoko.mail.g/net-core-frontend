import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserModel } from './_models/user';
import { AccountService } from './_services/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  baseUrl = 'https://localhost:5001/api/';

  title = 'Custom PYFA Application';
  users: any;

  constructor(private accountService: AccountService) {}
 
  ngOnInit(): void {
    this.setCurrentUser();
  }

  /*get current user from browser local storage*/
  setCurrentUser() {
    var user: any = localStorage.getItem('user');
    if(user != null){
      user = JSON.parse(localStorage.getItem('user') || '{}');
    }
    this.accountService.setCurrentUser(user);
  }
}
