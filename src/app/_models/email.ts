export interface EmailModel {
    id: number,
    emailAddress: string,
    publicId: number,
    appUserId: number
};