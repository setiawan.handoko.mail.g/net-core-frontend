import { Byte } from "@angular/compiler/src/util";
import { EmailModel } from "./email";

export interface UserInfoModel {
    username: string;
    token: string;
    passwordHash: Byte;
    passwordSalt: Byte;
    fullName: string;
    emails: EmailModel[]
};