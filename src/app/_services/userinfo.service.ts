import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserInfoModel } from '../_models/userinfo';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  baseUrl = environment.apiUrl;
  
  constructor(private http: HttpClient) { }

  getAllUsers() {
    return this.http.get<UserInfoModel[]>(this.baseUrl + "users");
  }

  getUser(username: string) {
    return this.http.get<UserInfoModel>(this.baseUrl + "users/byusername/" + username);
  }

  updateUser(userData: UserInfoModel){
    return this.http.put(this.baseUrl + 'users', userData);
  }

  deleteUser(username: string) {
    return this.http.delete(this.baseUrl + 'users/delete-data/' + username);
  }
}
