import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserModel } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = environment.apiUrl;

  private currentUserSource = new ReplaySubject<UserModel>(1); //size buffer = 1
  currentUser$ = this.currentUserSource.asObservable(); //currentuser

  constructor(private http: HttpClient) {}

  login(model: any){
    return this.http.post<UserModel>(this.baseUrl + 'account/login', model).pipe(
      map((response: UserModel) => {
        const user = response;
        if(user){
          localStorage.setItem('user', JSON.stringify(user)); //save to browser local storage
          this.currentUserSource.next(user); //update current user observable
        }
        return user;
      })
    );
  }

  logout() {
    localStorage.removeItem('user'); //remove browser local storage
    this.currentUserSource.next(); //set currentuser null
  }

  register(model: any) {
    return this.http.post<UserModel>(this.baseUrl + 'account/register', model).pipe(
      map((user: UserModel) => {
        if(user){
          localStorage.setItem('user', JSON.stringify(user));
          this.currentUserSource.next(user);
        }
        return user;
      })
    );
  }

  /* -- HELPERS -- */
  setCurrentUser(user: UserModel){
    this.currentUserSource.next(user);
  }
}
