import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from '../_services/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Input() usersFromHomeComponent: any; //get value from parent
  @Output() cancelRegister = new EventEmitter(); //send value to parent
  model: any = {};

  constructor(private accountService: AccountService, private toastr: ToastrService) { }

  ngOnInit(): void { }

  //function
  register() {
    this.accountService.register(this.model).subscribe(resp => {
      console.log(resp);
      this.cancel();
    }, err => {
      console.log(err);
      this.toastr.error(err.error);
    });
  }

  cancel() {
    this.cancelRegister.emit(false);
  }

}
