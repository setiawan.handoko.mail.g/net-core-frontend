import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { UserModel } from 'src/app/_models/user';
import { UserInfoModel } from 'src/app/_models/userinfo';
import { AccountService } from 'src/app/_services/account.service';
import { UserInfoService } from 'src/app/_services/userinfo.service';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})
export class MemberDetailComponent implements OnInit {
  user: UserInfoModel = {} as UserInfoModel
  userauth: UserModel = {} as UserModel

  constructor(private userInfoService: UserInfoService, 
              private accountService: AccountService, 
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private router: Router) {
    this.accountService.currentUser$.pipe(take(1)).subscribe(usr => this.userauth = usr)
  }

  ngOnInit(): void {
    this.loadUser();
  }

  loadUser() {
    this.userInfoService.getUser(this.route.snapshot.paramMap.get ('username') || '{}').subscribe(u => {
      this.user = u;
    })
  }

  updateUser() {
    this.userInfoService.updateUser(this.user).subscribe(() => {
      this.toastr.success('Data Updated!');
      this.router.navigate(['./members/']);
    })
  }
}
