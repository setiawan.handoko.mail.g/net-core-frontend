import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { UserInfoModel } from 'src/app/_models/userinfo';
import { AccountService } from 'src/app/_services/account.service';
import { UserInfoService } from 'src/app/_services/userinfo.service';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css']
})
export class MemberListComponent implements OnInit {
  usrs: Observable<UserInfoModel[]> = {} as Observable<UserInfoModel[]>

  constructor(private userInfoService: UserInfoService, 
              public accountService: AccountService,
              private toastr: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers() {
    this.usrs = this.userInfoService.getAllUsers();
  }

  deleteUser(username: string) {
    this.userInfoService.deleteUser(username).subscribe(() => {
      this.toastr.success('Data Deleted!');
      window.location.reload();
    })
  }
}
